-- liquibase formatted sql

-- changeset Torchez:1713705801181-1
CREATE SEQUENCE IF NOT EXISTS hibernate_sequence START WITH 1 INCREMENT BY 1;

-- changeset Torchez:1713705801181-2
CREATE TABLE profile
(
    id                  BIGINT       NOT NULL,
    account_id          BIGINT       NOT NULL,
    email               VARCHAR(255),
    full_name           VARCHAR(255) NOT NULL,
    city                VARCHAR(255),
    persist_date        TIMESTAMP WITHOUT TIME ZONE,
    link_site           VARCHAR(255),
    link_github         VARCHAR(255),
    link_vk             VARCHAR(255),
    about               VARCHAR(255),
    image_link          VARCHAR(255),
    last_redaction_date TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    nickname            VARCHAR(255),
    CONSTRAINT pk_profile PRIMARY KEY (id)
);

-- changeset Torchez:1713705801181-3
ALTER TABLE profile
    ADD CONSTRAINT uc_profile_accountid UNIQUE (account_id);

